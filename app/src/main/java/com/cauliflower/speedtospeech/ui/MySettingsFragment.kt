package com.cauliflower.speedtospeech.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.cauliflower.speedtospeech.R

class MySettingsFragment : PreferenceFragmentCompat() {

    companion object {
        val TAG: String = MySettingsFragment::class.java.simpleName
        fun newInstance() = MySettingsFragment()
    }

    private lateinit var speedPref: ListPreference
    private lateinit var speechPref: ListPreference
    private lateinit var unitSpeedPref: ListPreference
    private lateinit var darkModePref: ListPreference

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        val keySpeedUpdateFreq = getString(R.string.pref_key_min_speed_update_frequency)
        val keySpeechFreq = getString(R.string.pref_key_speech_frequency)
        val keyUnitSpeed = getString(R.string.pref_key_unit_speed)
        val keyDarkMode = getString(R.string.pref_key_dark_mode)

        speedPref = findPreference(keySpeedUpdateFreq)!!
        speechPref = findPreference(keySpeechFreq)!!
        unitSpeedPref = findPreference(keyUnitSpeed)!!
        darkModePref = findPreference(keyDarkMode)!!

        speedPref.summaryProvider = Preference.SummaryProvider<ListPreference> { preference ->
            val value = preference.value
            "$value s"
        }
        speechPref.summaryProvider = Preference.SummaryProvider<ListPreference> { preference ->
            val value = preference.value
            "$value s"
        }
        unitSpeedPref.summaryProvider = Preference.SummaryProvider<ListPreference> { preference ->
            preference.entry
        }
        darkModePref.summaryProvider = Preference.SummaryProvider<ListPreference> { preference ->
            preference.entry
        }

        darkModePref.setOnPreferenceChangeListener { _, newValue ->
            AppCompatDelegate.setDefaultNightMode(Integer.parseInt(newValue.toString()))
            true
        }
    }
}