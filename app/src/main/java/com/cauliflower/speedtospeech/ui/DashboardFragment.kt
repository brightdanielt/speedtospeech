package com.cauliflower.speedtospeech.ui

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.PorterDuff
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.cauliflower.speedtospeech.*
import com.cauliflower.speedtospeech.databinding.FragmentDashboardBinding
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult

/**
 * Show the user's speed on the screen.
 * A FAB to let user switch SpeechService.
 * */
class DashboardFragment : Fragment() {

    private lateinit var binding: FragmentDashboardBinding
    private var activityListener: Callbacks? = null
    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            updateSpeed(locationResult.lastLocation)
        }
    }
    private val speedAnalyst = SpeedAnalyst()

    private var unitSpeed: String = ""

    private lateinit var notificationBroadcastReceiver: BroadcastReceiver

    companion object {
        val TAG: String = DashboardFragment::class.java.simpleName
        fun newInstance() = DashboardFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is Callbacks) {
            activityListener = context

            // If fine location permission isn't approved, instructs the parent Activity to replace
            // this fragment with the permission request fragment.
            if (!context.hasPermission(ACCESS_FINE_LOCATION)) {
                activityListener?.requestFineLocationPermission()
            }
        } else {
            throw RuntimeException("$context must implement LocationUpdateFragment.Callbacks")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDashboardBinding.inflate(inflater, container, false)
        loadPref()
        binding.fabVoiceMode.setOnClickListener {
            if (!NotificationManagerCompat.from(requireContext()).areNotificationsEnabled()) {
                activityListener?.requestPostNotificationPermission()
                return@setOnClickListener
            }
            val channelId = getString(R.string.notification_channel_id_speech_mode)
            if (!requireContext().isNotificationChannelEnabled(channelId)) {
                activityListener?.requestNotificationChannelEnable(channelId)
                return@setOnClickListener
            }
            val speechService = Intent(requireContext(), SpeechService::class.java)
            if (isMyServiceRunning(requireContext(), SpeechService::class.java)) {
                requireContext().stopService(speechService)
                updateFab(true)
            } else {
                requireContext().startService(speechService)
                updateFab(false)
            }
        }
        return binding.root
    }

    @SuppressLint("MissingPermission")
    override fun onStart() {
        super.onStart()
        updateFab(!isMyServiceRunning(requireContext(), SpeechService::class.java))
        MyLocationManager.getInstance(requireContext()).startLocationUpdates(locationCallback)
        initBroadcastReceiver()
    }

    override fun onStop() {
        super.onStop()
        requireContext().unregisterReceiver(notificationBroadcastReceiver)
        MyLocationManager.getInstance(requireContext()).stopLocationUpdates(locationCallback)
    }

    /**
     * Receive #SpeechService.ACTION_CLOSE_NOTIFICATION to update ui state.
     * */
    private fun initBroadcastReceiver() {
        notificationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == SpeechService.ACTION_CLOSE_NOTIFICATION)
                    updateFab(true)
            }
        }
        IntentFilter().apply {
            addAction(SpeechService.ACTION_CLOSE_NOTIFICATION)
            requireContext().registerReceiver(notificationBroadcastReceiver, this)
        }
    }

    private fun updateFab(isStopNow: Boolean) {
        if (isStopNow) {
            binding.fabVoiceMode.setImageResource(R.drawable.ic_baseline_record_voice_over_24)
        } else {
            val fabSrc = getDrawable(
                requireContext(), R.drawable.ic_baseline_voice_over_off_24
            )
            val willBeWhite = fabSrc?.constantState?.newDrawable()
            val color = ResourcesCompat.getColor(
                resources, R.color.fabStopVoiceServiceColor, null
            )
            willBeWhite?.mutate()?.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
            binding.fabVoiceMode.setImageDrawable(willBeWhite)
        }
    }

    private fun loadPref() {
        val keyUnitSpeed = getString(R.string.pref_key_unit_speed)
        unitSpeed = valueFromSharedPreferences(requireContext(), keyUnitSpeed)
            ?: getString(R.string.pref_unit_speed_df)
    }

    private fun updateSpeed(location: Location?) {
        if (location != null) {
            speedAnalyst.addSpeed(location.speed)
            speedAnalyst.getLatestSpeed(unitSpeed).let {
                binding.tvSpeedValue.text = it.toString()
                binding.tvUnit.text = unitSpeed
                Log.i(TAG, "$it $unitSpeed")
            }
        } else {
            Log.e("", "無法獲取地理信息")
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface Callbacks {
        fun requestFineLocationPermission()
        fun requestBackgroundLocationPermission()
        fun requestPostNotificationPermission()
        fun requestNotificationChannelEnable(channelId: String)
    }
}