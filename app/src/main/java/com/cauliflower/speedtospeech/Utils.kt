package com.cauliflower.speedtospeech

import android.Manifest
import android.app.ActivityManager
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import java.math.RoundingMode
import java.text.DecimalFormat

/**
 * Check the specific notification channel is not blocked.

 * @param id Id of notification channel.
 * @return True, if the channel is not blocked.
 * */
fun Context.isNotificationChannelEnabled(id: String): Boolean {
    val manager = NotificationManagerCompat.from(this)
    return manager.getNotificationChannel(id)?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            it.importance != NotificationManager.IMPORTANCE_NONE
        } else {
            manager.areNotificationsEnabled()
        }
    } ?: false
}

/**
 * Check the permission of POST_NOTIFICATION and the specific notification channel is not blocked.
 *
 * Notice:[NotificationManagerCompat.areNotificationsEnabled] only indicates the permission is
 * granted or not and doesn't indicate if a notification channel is blocked or not.
 *
 * @param id Id of notification channel.
 * @return True, if permission is granted and the channel is not blocked.
 * */
fun Context.canPostNotificationChannel(id: String): Boolean {
    return NotificationManagerCompat.from(this)
        .areNotificationsEnabled() && isNotificationChannelEnabled(id)
}

/**
 * Helper functions to simplify permission checks/requests.
 */
fun Context.hasPermission(permission: String): Boolean {

    // Background permissions didn't exit prior to Q, so it's approved by default.
    if (permission == Manifest.permission.ACCESS_BACKGROUND_LOCATION &&
        android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.Q
    ) {
        return true
    }

    return ActivityCompat.checkSelfPermission(this, permission) ==
            PackageManager.PERMISSION_GRANTED
}

/**
 * Requests permission and if the user denied a previous request, but didn't check
 * "Don't ask again", we provide additional rationale.
 *
 * Note: The Snackbar should have an action to request the permission.
 */
fun Fragment.requestPermissionWithRationale(
    permission: String,
    requestCode: Int,
    snackbar: Snackbar
) {
    val provideRationale = shouldShowRequestPermissionRationale(permission)

    if (provideRationale) {
        snackbar.show()
    } else {
        requestPermissions(arrayOf(permission), requestCode)
    }
}

/**
 * Convert speed from (meters/second) to (km/hour).
 * @param speed in meters/second over ground
 * */
fun toKmPerHour(speed: Float): Float {
    //Convert to speed in km/hour over ground.
    return speed * 3.6f
}

/**
 * Rounding to two decimal places.
 * */
fun roundToTwoDecimalPlaces(value: Float): Float {
    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.HALF_UP
    return df.format(value).toFloat()
}

/**
 * This method only check the caller's own services.
 * @param serviceClass the Service to check
 * @return true if the service is running, and vice versa
 * */
fun isMyServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
    val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
    for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
        if (serviceClass.name == service.service.className) {
            return true
        }
    }
    return false
}

fun valueFromSharedPreferences(context: Context, key: String): String? {
    return PreferenceManager.getDefaultSharedPreferences(context).getString(key, null)
}

fun intFromSharedPreferences(context: Context, key: String): Int {
    return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, 0)
}