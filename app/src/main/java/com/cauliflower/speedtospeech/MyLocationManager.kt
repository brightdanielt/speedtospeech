package com.cauliflower.speedtospeech

import android.content.Context
import android.os.Looper
import android.util.Log
import com.google.android.gms.location.*
import java.util.concurrent.TimeUnit

/**
 * Manages all location related tasks for the app.
 */
class MyLocationManager private constructor(context: Context) {

    companion object {
        val TAG: String = MyLocationManager::class.java.simpleName
        private var INSTANCE: MyLocationManager? = null

        fun getInstance(context: Context): MyLocationManager {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: createInstance(context)
            }
        }

        private fun createInstance(context: Context): MyLocationManager {
            return MyLocationManager(context.applicationContext).also { INSTANCE = it }
        }
    }

    private val fusedLocationClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    private val locationRequest: LocationRequest = LocationRequest.create().apply {
        val keySpeedUpdateFreq = context.getString(R.string.pref_key_min_speed_update_frequency)
        val updateFreq = (valueFromSharedPreferences(context, keySpeedUpdateFreq)
            ?: context.getString(R.string.pref_speed_update_frequency_df)).toLong()

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        fastestInterval = TimeUnit.SECONDS.toMillis(updateFreq)

        // Sets the desired interval for active location updates. This interval is inexact. You
        // may not receive updates at all if no location sources are available, or you may
        // receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        //
        // IMPORTANT NOTE: Apps running on "O" devices (regardless of targetSdkVersion) may
        // receive updates less frequently than this interval when the app is no longer in the
        // foreground.
        interval = TimeUnit.SECONDS.toMillis(10)

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        maxWaitTime = TimeUnit.SECONDS.toMillis(30)

        priority = Priority.PRIORITY_HIGH_ACCURACY
    }

    fun startLocationUpdates(callback: LocationCallback) {
        Log.d(TAG, "startLocationUpdates()")
        try {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                callback,
                Looper.getMainLooper()
            )
        } catch (permissionRevoked: SecurityException) {
            // Exception only occurs if the user revokes the FINE location permission before
            // requestLocationUpdates() is finished executing (very rare).
            Log.d(TAG, "Location permission revoked; details: $permissionRevoked")
            throw permissionRevoked
        }
    }

    fun stopLocationUpdates(callback: LocationCallback) {
        Log.d(TAG, "stopLocationUpdates()")
        fusedLocationClient.removeLocationUpdates(callback)
    }

}