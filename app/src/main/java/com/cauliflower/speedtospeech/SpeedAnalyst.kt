package com.cauliflower.speedtospeech

import java.lang.IllegalArgumentException

/**
 * Store speeds from [android.location.Location.getSpeed]
 * and translate them into useful information.
 * */
class SpeedAnalyst {

    companion object {
        val TAG: String = SpeedAnalyst::class.java.simpleName
        const val INVALID_SPEED = -1f
    }

    private val speedList = arrayListOf<Float>()

    private var fastestSpeed: Float = INVALID_SPEED

    fun addSpeed(speed: Float) {
        speedList.add(speed)
        if (fastestSpeed < speed) fastestSpeed = speed
    }

    fun getLatestSpeed(unit: String = "m/s"): Float {

        if (speedList.isEmpty()) return INVALID_SPEED

        val last = when (unit) {
            "m/s" -> speedList.last()
            "km/h" -> toKmPerHour(speedList.last())
            else -> throw IllegalArgumentException("Illegal unit of speed:$unit")
        }

        return roundToTwoDecimalPlaces(last)
    }

    fun getFastestSpeed(unit: String = "m/s"): Float {

        if (speedList.isEmpty()) return INVALID_SPEED

        val theFast = when (unit) {
            "m/s" -> fastestSpeed
            "km/h" -> toKmPerHour(fastestSpeed)
            else -> throw IllegalArgumentException("Illegal unit of speed:$unit")
        }

        return roundToTwoDecimalPlaces(theFast)
    }


    fun getAverageSpeed(unit: String = "m/s"): Float {

        if (speedList.isEmpty()) return INVALID_SPEED

        val avg = when (unit) {
            "m/s" -> speedList.average().toFloat()
            "km/h" -> toKmPerHour(speedList.average().toFloat())
            else -> throw IllegalArgumentException("Illegal unit of speed:$unit")
        }

        return roundToTwoDecimalPlaces(avg)
    }

    fun resetSpeed() {
        speedList.clear()
        fastestSpeed = INVALID_SPEED
    }
}