package com.cauliflower.speedtospeech

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.NotificationManagerCompat

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        loadPrefs()
        createNotificationChannel()
    }

    private fun loadPrefs() {
        val darkModeValue = valueFromSharedPreferences(this, getString(R.string.pref_key_dark_mode))
            ?: getString(R.string.pref_dark_mode_df)
        AppCompatDelegate.setDefaultNightMode(Integer.valueOf(darkModeValue))
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel(
                getString(R.string.notification_channel_id_speech_mode),
                getString(R.string.notification_channel_name_speech_mode),
                NotificationManager.IMPORTANCE_LOW
            ).apply {
                NotificationManagerCompat.from(this@MyApplication)
                    .createNotificationChannel(this)
            }
        }
    }
}