package com.cauliflower.speedtospeech

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.os.*
import android.speech.tts.TextToSpeech
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.cauliflower.speedtospeech.SpeedAnalyst.Companion.INVALID_SPEED
import com.cauliflower.speedtospeech.ui.MainActivity
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import java.util.*

/**
 * Features:
 * ➤ Start a foreground notification which updates the user's speed.
 *    There are three buttons in notification:
 *    • Reset speed
 *    • Mute Speed
 *    • Close Notification
 * ➤ Use the TextToSpeech to speak the user's speed.
 * */
class SpeechService : Service() {

    private lateinit var tts: TextToSpeech
    private lateinit var myLocationManager: MyLocationManager
    private val locationCallback: LocationCallback by lazy {
        object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                updateSpeed(locationResult.lastLocation)
            }
        }
    }
    private lateinit var notificationBuilder: NotificationCompat.Builder
    private lateinit var notificationManager: NotificationManager
    private lateinit var remoteViews: RemoteViews

    //Receive actions on notification.
    private lateinit var notificationBroadcastReceiver: BroadcastReceiver

    private val speedAnalyst = SpeedAnalyst()
    private var isMute: Boolean = false

    //Pref
    //One time per N seconds.
    private var speechFreq: Long = 0
    private var speechVolume: Float = 0f
    private var unitOfSpeed: String = ""

    private val speechHandler = Handler(Looper.myLooper()!!)
    private val speechRunnable = object : Runnable {
        override fun run() {
            speedAnalyst.getLatestSpeed(unitOfSpeed).let {

                if (isMute || it == INVALID_SPEED) return@let

                speak("$it$unitOfSpeed")

                Log.i(TAG, "speak: $it $unitOfSpeed every ${speechFreq}s")
            }
            speechHandler.postDelayed(this, speechFreq * 1000)
        }
    }

    companion object {
        val TAG: String = SpeechService::class.java.simpleName
        const val REQUEST_CODE_RESET_SPEED = 1
        const val REQUEST_CODE_MUTE = 2
        const val REQUEST_CODE_CLOSE_NOTIFICATION = 3
        const val ACTION_RESET_SPEED = "reset_speed"
        const val ACTION_MUTE = "mute"
        const val ACTION_CLOSE_NOTIFICATION = "close_notification"
    }

    override fun onBind(intent: Intent?): IBinder? {
        //Not a bind service
        return null
    }

    override fun onCreate() {
        super.onCreate()
        myLocationManager = MyLocationManager.getInstance(this)
        loadPrefs()
        initNotification()
        initTTS()
        initSpeechHandler()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        myLocationManager.startLocationUpdates(locationCallback)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        myLocationManager.stopLocationUpdates(locationCallback)
        unregisterReceiver(notificationBroadcastReceiver)
        speechHandler.removeCallbacks(speechRunnable)
        tts.stop()
        tts.shutdown()
        stopForeground(true)
    }

    private fun loadPrefs() {
        val keySpeechFreq = getString(R.string.pref_key_speech_frequency)
        val keySpeechVolume = getString(R.string.pref_key_speech_volume)
        val keyUnitSpeed = getString(R.string.pref_key_unit_speed)
        speechFreq = (valueFromSharedPreferences(this, keySpeechFreq)
            ?: getString(R.string.pref_speech_frequency_df)).toLong()
        speechVolume = intFromSharedPreferences(this, keySpeechVolume).toFloat()
        unitOfSpeed = valueFromSharedPreferences(this, keyUnitSpeed)
            ?: getString(R.string.pref_unit_speed_df)
    }

    private fun initTTS() {
        tts = TextToSpeech(this) { status ->
            if (status == TextToSpeech.SUCCESS) {
                tts.language = Locale.getDefault()
            }
        }
    }

    private fun initNotification() {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // Create an Intent for the activity you want to start
        val mainIntent = Intent(this, MainActivity::class.java)
        // Create the TaskStackBuilder
        val mainPendingIntent: PendingIntent? = TaskStackBuilder.create(this).run {
            // Add the intent, which inflates the back stack
            addNextIntentWithParentStack(mainIntent)
            // Get the PendingIntent containing the entire back stack
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE)
            } else {
                getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            }
        }
        initRemoteViews()

        notificationBuilder =
            NotificationCompat.Builder(this, getString(R.string.notification_channel_id_speech_mode))
                .setContentTitle(getText(R.string.notification_title_speech_mode))
                .setSmallIcon(R.drawable.ic_baseline_record_voice_over_24)
                .setContentIntent(mainPendingIntent)
                .setCustomContentView(remoteViews)
                .setOnlyAlertOnce(true)

        startForeground(1, notificationBuilder.build())
    }

    private fun initRemoteViews() {
        remoteViews = RemoteViews(packageName, R.layout.notification)

        Intent(ACTION_RESET_SPEED).apply {
            val pendingReset = PendingIntent.getBroadcast(
                this@SpeechService,
                REQUEST_CODE_RESET_SPEED,
                this,
                PendingIntent.FLAG_IMMUTABLE
            )
            remoteViews.setOnClickPendingIntent(R.id.btn_notification_reset, pendingReset)
        }

        Intent(ACTION_MUTE).apply {
            val pendingMute = PendingIntent.getBroadcast(
                this@SpeechService,
                REQUEST_CODE_MUTE,
                this,
                PendingIntent.FLAG_IMMUTABLE
            )
            remoteViews.setOnClickPendingIntent(R.id.btn_notification_mute, pendingMute)
        }

        Intent(ACTION_CLOSE_NOTIFICATION).apply {
            val pendingClose = PendingIntent.getBroadcast(
                this@SpeechService,
                REQUEST_CODE_CLOSE_NOTIFICATION,
                this,
                PendingIntent.FLAG_IMMUTABLE
            )
            remoteViews.setOnClickPendingIntent(R.id.btn_notification_close, pendingClose)
        }
        initBroadcastReceiver()
    }

    private fun initBroadcastReceiver() {
        notificationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                when (intent.action) {
                    ACTION_RESET_SPEED -> actionReset()
                    ACTION_MUTE -> actionMute()
                    ACTION_CLOSE_NOTIFICATION -> stopSelf(1)
                    else -> {}
                }
            }

            private fun actionReset() {
                speedAnalyst.resetSpeed()
                updateNotificationMsg("目前 0.00 $unitOfSpeed \n平均 0.00 $unitOfSpeed")
            }

            private fun actionMute() {
                isMute = !isMute
                val imgId = if (isMute) R.drawable.ic_baseline_volume_off_24
                else R.drawable.ic_baseline_volume_on_24
                remoteViews.setImageViewResource(R.id.btn_notification_mute, imgId)
                notificationManager.notify(1, notificationBuilder.build())
            }
        }
        IntentFilter().apply {
            addAction(ACTION_RESET_SPEED)
            addAction(ACTION_MUTE)
            addAction(ACTION_CLOSE_NOTIFICATION)
            registerReceiver(notificationBroadcastReceiver, this)
        }
    }

    private fun initSpeechHandler() {
        speechHandler.postDelayed(speechRunnable, 1000)
    }

    private fun updateSpeed(location: Location?) {
        if (location != null) {
            speedAnalyst.addSpeed(location.speed)
            val msg = "目前 ${speedAnalyst.getLatestSpeed(unitOfSpeed)} $unitOfSpeed \n" +
                    "平均 ${speedAnalyst.getAverageSpeed(unitOfSpeed)} $unitOfSpeed"
            updateNotificationMsg(msg)
            Log.i(TAG, msg)
        } else {
            Log.e(TAG, "無法獲取地理信息")
        }
    }

    private fun updateNotificationMsg(message: String) {
        remoteViews.setTextViewText(R.id.tv_notification_speed, message)
        notificationManager.notify(1, notificationBuilder.build())
    }

    private fun speak(text: String) {
        if (isMute) return
        val bundle = Bundle()
        bundle.putFloat(TextToSpeech.Engine.KEY_PARAM_VOLUME, speechVolume / 100)
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, bundle, "tts1")
        Log.i(TAG, "volume:${speechVolume / 100}")
    }
}