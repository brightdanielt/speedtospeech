Android SpeedToSpeech demo(Kotlin)
======================================
Demo demonstrating how to request permissions, request location update and notify location information<br>
with a notification and TextToSpeech.

Introduce
----------------------
- Permissions<br>
  In Android 6.0 (API level 23) or higher, developers must request the dangerous permissions at runtime<br>
  by following the steps in [Request app permissions], this project refer to [LocationUpdatesBackgroundKotlin]<br>
  to fulfill the permissions feature.<br>

- Location<br>
  This application gets location info from Android Location Service, however, according to [Android : LocationManager vs Google Play Services], developers are strongly encouraged to switch to the Google Play services location APIs as soon as possible, another interesting thing is Microsoft doc [Location services on Android] also recommends developers to use fused location provider.<br>
  Other references:<br>
  [Choice between Google Play Location Service and Android Location Service]<br>
  [Fused Location Provider API]<br>

  ⭐︎ According to the documents and posts above, the advantages of the Google play location service are:
  <br>
  - Better developer/user experience:<br>
    In the previous location api, to define precise location, developers had to switch between network and GPS location providers (as GPS doesn't work indoors), now , it intelligently determines the best way to obtain the location of the devices based on what providers are available and how the device is being used.

  - Battery-efficient:<br>
    With new feature "Activity Recognition", Fused Location Provider uses different sensors to adjust the necessary location updates frequency.
    As described in the Google Fused location provider API documentation:<br>
    _"you can request the most accurate data available, or the best accuracy possible with no additional power consumption"_

  - New feature - Geofencing APIs:<br>
    It can notify apps when a user is entering or leaving some define area with optimized for battery, and up to 100 targets per apps.

- Notification<br>
  This application notifies user's location info with a notification which is bound to a foreground service.<br>
  Check [Services overview] and [Notifications Overview] for more information.

- TextToSpeech<br>
  It's really simple and easy, check [Android doc - TextToSpeech] and [Text to Speech Converter using Android].

Developer's note
------------------------
2022/08/09<br>
Try to handle speed data in one custom object to simplify logic in SpeechService,
the threshold of this decision is that the logic of calculating speed has been complicate enough to reduce
the readability of SpeechService. There should be these variables in the new object "SpeedRecorder":<br>
- val speedList<br>
  The list every new speed should be added.

- var currentSpeed<br>
  The latest speed in the speedList.

- var fastSpeed<br>
  The fast speed in the speedList.

- var avgSpeed<br>
  Average value of speedList.

[Request app permissions]:https://developer.android.com/training/permissions/requesting

[Choice between Google Play Location Service and Android Location Service]:https://antoniohongkr.wordpress.com/2013/08/19/google-play-service-analysis-4-choice-between-google-play-location-service-and-android-location-service/

[Android : LocationManager vs Google Play Services]:https://stackoverflow.com/questions/33022662/android-locationmanager-vs-google-play-services

[Location services on Android]:https://docs.microsoft.com/zh-tw/xamarin/android/platform/maps-and-location/location

[Fused Location Provider API]:https://developers.google.com/location-context/fused-location-provider

[LocationUpdatesBackgroundKotlin]:https://github.com/android/location-samples/tree/master/LocationUpdatesBackgroundKotlin

[Receive location updates in Android 10 with Kotlin]:https://developer.android.com/codelabs/while-in-use-location?hl=en#0

[Services overview]:https://developer.android.com/guide/components/services

[Notifications Overview]:https://developer.android.com/guide/topics/ui/notifiers/notifications

[TTS]:https://medium.com/@rtficial/speech-to-text-and-text-to-speech-with-android-85758ff0f6d3

[ForegroundService]:https://developer.android.com/guide/components/foreground-services

[Text to Speech Converter using Android]:https://androidapps-development-blogs.medium.com/how-to-convert-text-to-speech-in-android-studio-text-to-speech-converter-using-android-c62d5d186524

[Android doc - TextToSpeech]:https://developer.android.com/reference/android/speech/tts/TextToSpeech